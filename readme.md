# GitLab Inactive Users

[![pipeline status](https://gitlab.com/christiaanconover/gitlab-inactive-users/badges/master/pipeline.svg)](https://gitlab.com/christiaanconover/gitlab-inactive-users/-/commits/master)

This utility simplifies the process of blocking or deactivating GitLab users that have not had any activity in a given amount of time.
